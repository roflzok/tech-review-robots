var videoCanPlay = true;
var isVideo = true;


// Robots fixing and unfixing to top of viewport 
function stickyRobot(id) {
    // try to define your vars at the top of the func
    var wrap;
    var small;
    var topBoundary;
    var bottomBoundary;
    var fixedPosition;

    // jquery object, even if empty will always return true, $(smth)[id] howerver points to the first DOM element in that object, if nothing was found [0] will return undefined
    if ($(id)[0]) {
        id = id + ' ';

        // always precache you jquery obj, otherwise you're slowing your app down doing jquery DOM search over and over;
        wrap = $(id + '.affix-wrap');
        small = $(id + 'small');

        topBoundary = wrap.offset().top;
        bottomBoundary = small.offset().top - wrap.height() - small.height();
        fixedPosition = bottomBoundary - topBoundary;

        $(document).bind('ready scroll', function() {
            var docScroll = $(document).scrollTop();
            if (docScroll < topBoundary) {
                wrap.removeClass("fix").fadeOut("slow");
            } else if (docScroll >= topBoundary && docScroll < bottomBoundary) {
                wrap.removeClass("fixed").addClass('fix').css("top", "40px").fadeIn("slow");
            } else if (docScroll >= bottomBoundary) {
                wrap.removeClass("fix").addClass('fixed').css("top", fixedPosition);
            }
        });

    } else {

        console.log('we didnt find that id: ' + id);

    }
};




// Background videos
function videoBG(id, file, poster) {
    var poster = poster ? "assets/videos/fallback/" + poster : '';
	
    var videobackground = new $.backgroundVideo($(id), {
        "align": "centerXY",
        "path": "assets/videos/",
        "poster": poster,
        "width": 630,
        "height": 360,
        "filename": file,
        "types": ["mp4", "webm", "ogg"]
    });

    // $(window).resize().scroll();

    $(window).load(function() {
        $('html').addClass('loaded');
        // $(window).resize().scroll();
    });

    return videobackground.$videoEl;
}


    


// Get various measurements and dimensions at doc.ready; do this early to avoid dependency issues
function docReadySetup() {
    // Get height offsets for Part 1
    var wrapHeight = $('#partOne .wrap').height();
    $('#partOne .wrap').css("height", wrapHeight);

    // Get height offsets for Part 3
    var wrapHeight = $('#partThree .wrap').height();
    $('#partThree .wrap').first().css("height", wrapHeight);

    var videoHeight = $('#desert > video').height();
    $("#jumper").css("height", videoHeight);
    $('#petman > video').css("height",videoHeight);


    // Get fixed elements' widths so they don't resize on affix
    $('.affix-wrap').each(function() {
        var affixWidth = $(this).width();
        $(this).css("width", affixWidth);
    });

    $("#jumper").hide();
	
	if(videoCanPlay){
	    $("#beach > video").hide()[0].pause();
	    $("#petman > video").hide()[0].pause();
	} else {
	    $("#beach .fallback").hide()[0];
	    $("#petman .fallback").hide()[0];
	}
};


// Background Waypoints

function backgroundWaypoints() {

    // Intro
    $("#intro").waypoint(function(direction) {
        if (direction === 'down') {
            if(videoCanPlay)
                $("#desert > video").hide()[0].pause();
            else
                $("#desert > img").hide();
        }
    }, {
        offset: '-20%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            if(videoCanPlay)
                $("#desert > video").show()[0].play();
            else
                $("#desert > img").show();
        }
    }, {
        offset: '-20%'
    });



    // Part One
    $("#partOne").waypoint(function(direction) {
        if (direction === 'down') {
            $("#jumper").show();
        }
    }, {
        offset: '120%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            $("#jumper").hide();
        }
    }, {
        offset: '120%'
    });

    $("#partOne > .bg").waypoint(function(direction) {
        if (direction === 'down') {
            $("#jumper").hide();
        }
    }, {
        offset: '-20%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            $("#jumper").show();
        }
    }, {
        offset: '-20%'
    });





    // Part Two
    $("#partTwo").waypoint(function(direction) {
        if (direction === 'down') {
            if(videoCanPlay)
                $("#beach > video").show()[0].play();
            else
                $("#beach > img").show();
        }   
    }, {
        offset: '120%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            if(videoCanPlay)
                $("#beach > video").hide()[0].pause();
            else
                $("#beach > img").hide();
        }
    }, {
        offset: '120%'
    });

    $("#partTwo > .bg").waypoint(function(direction) {
        if (direction === 'down') {
            if(videoCanPlay)
                $("#beach > video").hide()[0].pause();
            else
                $("#beach > img").hide();
        }
    }, {
        offset: '-20%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            if(videoCanPlay)
                $("#beach > video").show()[0].play();
            else
                $("#beach > img").show();
        }
    }, {
        offset: '-20%'
    });




    // Part Three
    $("#partThree").waypoint(function(direction) {
        if (direction === 'down') {
            if(videoCanPlay)
                $("#petman > video").show()[0].play();
            else
                $("#petman > img").show();
        }
    }, {
        offset: '120%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            if(videoCanPlay)
                $("#petman > video").hide()[0].pause();
            else
                $("#petman > img").hide();
        }
    }, {
        offset: '120%'
    });

    $("#partThree > .bg").waypoint(function(direction) {
        if (direction === 'down') {
            if(videoCanPlay)
                $("#petman > video").hide()[0].pause();
            else
                $("#petman > img").hide();
        }
    }, {
        offset: '-20%'
    }).waypoint(function(direction) {
        if (direction === 'up') {
            if(videoCanPlay)
                $("#petman > video").show()[0].play();
            else
                $("#petman > img").show();
        }
    }, {
        offset: '-20%'
    });
}


function startTheShow() {
    // $(".loading div").hide();
    window.setTimeout(function (){ $('.loading div').fadeOut(500); }, 4500);
    window.setTimeout(function (){ $('.loading').fadeOut(2500); }, 6000);
    window.setTimeout(function (){ $('#title h1').fadeIn(2500); }, 6000);
    // $(".loading").fadeOut(3500);
    // $("#title h1").fadeIn(3500);
}





$(document).ready(function() {

    var videos = [{
        div: '#desert',
        path: 'desert',
        placeholder: 'desert.jpg'
    }, {
        div: '#beach',
        path: 'beach',
        placeholder: 'beach.jpg'
    }, {
        div: '#petman',
        path: 'petman',
        placeholder: 'petman.jpg'
    }];

    var i = 0,
        loaded = 0,
        $videoEl;

    // you can wrap everything in another div with some id and hide it instead of the body, so that you can show spinning wheel or smth
    // like that instead
    var body = $('.content').hide();

    // mobile sniffing properties
    (function(a) {
        (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
    })(navigator.userAgent || navigator.vendor || window.opera);


    while (i < videos.length) {
        if (jQuery.browser.mobile || navigator.userAgent.match(/iPad/i) != null || navigator.userAgent.match(/SAMSUNG/i) != null) {
            // if we don't play vids - don't provide the urls, only posters
            videoBG(videos[i].div, '', videos[i].placeholder);
            videoCanPlay = false;

            if (i === videos.length - 1) {
                body.show();
                loadPage();
            }

            $('video').css({"minHeight" : "568px"});
        } else {
            $videoEl = videoBG(videos[i].div, videos[i].path);

            // listening to videos, waiting till they are preloaded and able to be played through the end
            $videoEl.on('canplaythrough', function() {

                if (++loaded === videos.length) {
                    body.show();
                    loadPage();
                }
            });
        }

        i++;
    }



    function loadPage() {
        // Set up SmoothScroll
        $('.smooth-scroll').smoothScroll();

        docReadySetup();
        backgroundWaypoints();


        stickyRobot('#hopper');
        stickyRobot('#biped');
        stickyRobot('#atlas');



        

        $('video').each(function(idx) {
            if (idx === 0) {
                this.play();
            } else {
                this.pause();
            }
        });


        startTheShow();



        // Google Analytics scrolling depth
        /*$.scrollDepth({
        elements: ['.skills', '.recent-projects', '.recent-designs', 'footer']
        });*/
    }

    // code here for when the window resizes
    // $(window).resize(function(e){ 

    // });
})