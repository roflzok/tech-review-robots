################################################
# COMPASS PLUGINS
# Require any additional compass plugins here
################################################

#require 'bourbon-compass'
#require "compass-growl"
require "compass-notify"




################################################
# COMPASS SETTINGS
# Set this to the root of your project when deployed:
################################################

http_path = "/"
css_dir = "assets/css"
sass_dir = "assets/sass"
images_dir = "assets/imgs"
javascripts_dir = "assets/js"
fonts_dir = "assets/fonts" 

output_style = :nested
environment = :development




################################################
# COMPASS-NOTIFY
# Pipes pass/fail notifications to OS X
################################################

on_stylesheet_saved do |filename|
  CompassNotify.notify('SUCCESS!', 'Shit is updated, son!')
end

on_stylesheet_error do |filename, message|
  CompassNotify.notify('ERROR!!', 'ruh-roh, dude...')
end